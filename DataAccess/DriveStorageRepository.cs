using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using API.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Drive.v3.Data;
using Google.Apis.Services;

namespace API.DataAccess
{
    public class DriveStorageRepository
    {
        private static DriveService service;
        public static void Initialize()
        {
            service = CreateService();
        }

        private static DriveService CreateService()
        {
            using (var stream =
                new FileStream("credentials-google-api.json", FileMode.Open, FileAccess.Read))
            {
                var credentials = GoogleCredential.FromStream(stream);
                if (credentials.IsCreateScopedRequired)
                {
                    credentials = credentials.CreateScoped(new string[] { DriveService.Scope.Drive });
                }


                return new DriveService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credentials,
                    ApplicationName = "application name",
                });
            }
        }

        public void UploadImage(Stream stream, string FileName)
        {
            var fileMetadata = new Google.Apis.Drive.v3.Data.File();
            fileMetadata.Name = FileName;
            fileMetadata.MimeType = "image/jpeg";
            FilesResource.CreateMediaUpload request;
            request = service.Files.Create(fileMetadata, stream,"image/jpeg");
            request.Fields = "id";
            request.Upload();

            var file = request.ResponseBody;

            AddPermision(file.Id);
        }

        public string GetImageIdFromName(string fileName)
        {
            string id = null;

            // List files.
            var listRequest = service.Files.List();
            listRequest.PageSize = 100;
            listRequest.Fields = "nextPageToken, files(id, name)";
            var files = listRequest.Execute().Files;
            foreach(var file in files)
                if(file.Name == fileName)
                    id = file.Id;

            if(id == null) return null;
            return $"https://drive.google.com/uc?export=view&id={id}";

        }

        private void AddPermision(string id)
        {
            Permission perm = new Permission();
            perm.Type = "anyone";
            perm.Role = "reader";

            try
            {
                service.Permissions.Create(perm, id).Execute();
            }
            catch (Exception e)
            {
                Console.WriteLine("An error occurred: " + e.Message);
            }
        }

        private static void DeleteFile(string id, DriveService service)
        {
            var request = service.Files.Delete(id);
            request.Execute();
        }

        // private static void CreateFolder(string folderName,DriveService service)
        // {
        //     var fileMetadata = new Google.Apis.Drive.v3.Data.File()
        //     {
        //         Name = folderName,
        //         MimeType = "application/vnd.google-apps.folder"
        //     };
        //     var request = service.Files.Create(fileMetadata);
        //     request.Fields = "id";
        //     var file = request.Execute();
        //     riteLine("Folder ID: " + file.Id);

        // }

        // private void ListFiles()
        // {
        //     FilesResource.ListRequest listRequest = service.Files.List();
        //     listRequest.PageSize = 10;
        //     listRequest.Fields = "nextPageToken, files(id, name)";
        //     // List files.
        //     IList<Google.Apis.Drive.v3.Data.File> files = listRequest.Execute()
        //         .Files;

        //     foreach(var file in files)
        //         riteLine(file.Id);
        // }
    }
}
